#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 Fondazione Bruno Kessler
# Author(s): Cristina Costa (ccosta@fbk.eu)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.
"""Web Socket LoMM test client.

usage: websocket_test_client.py [-h] [-f FILENAME] [-l LGTW_EUID] [-a LNS_IP]
                                [-p LNS_PORT]

Web Socked LoMM client.

optional arguments:
  -h, --help            show this help message and exit
  -f FILENAME, --filename FILENAME
                        Dataset file
  -l LGTW_EUID, --lgtw_euid LGTW_EUID
                        LoRaWAN Gateway euid
  -a LNS_IP, --lns_ip LNS_IP
                        LoRaWAN LNS IP address
  -p LNS_PORT, --lns_port LNS_PORT
                        LoRaWAN LNS IP port
"""

import json
import time
import datetime
from websocket import create_connection
from argparse import ArgumentParser
from websocket._exceptions import WebSocketConnectionClosedException

SLEEP_TIME = 2

LGTW_EUID = "b827:ebff:fee7:7681"
DATASET_FILE = './dataset/lorawan_capture.json'
VERSION = {
        "msgtype": "version",
        "firmware": "v1.0",
        "package": "alpha",
        "model": "Basic Station",
        "protocol": "2",
        "features": "xx"
        }

LNS_IP = "127.0.0.1"
LNS_PORT = 6039
CONNECT_STRING = "ws://{0}:{1}/router-{2}"
# default value: "ws://127.0.0.1:6039/router-"


def main():
    """ Parse argument list and execute client. """
    parser = ArgumentParser(description='Web Socked LoMM client.')
    parser.add_argument('-f', '--filename', dest='filename',
                        default=DATASET_FILE,
                        help='Dataset file')
    parser.add_argument('-l', '--lgtw_euid', dest='lgtw_euid',
                        default=LGTW_EUID,
                        help='LoRaWAN Gateway euid')
    parser.add_argument('-a', '--lns_ip', dest='lns_ip',
                        default=LNS_IP,
                        help='LoRaWAN LNS IP address')
    parser.add_argument('-p', '--lns_port', dest='lns_port',
                        default=LNS_PORT,
                        help='LoRaWAN LNS IP port')

    args = parser.parse_args()

    # Read JSON data into the packets variable
    with open(args.filename, 'r') as f:
        packets = json.load(f)

    # Prepare Web Socket connection string
    connect_string = CONNECT_STRING.format(args.lns_ip,
                                           args.lns_port,
                                           args.lgtw_euid)

    try:
        ws = create_connection(connect_string)
    except ConnectionRefusedError:
        print("ConnectionRefusedError: check IP address and Port number")
        exit()

    lgtw_version = VERSION
    lgtw_version["station"] = args.lgtw_euid

    print("Sending version request to %s\n" % connect_string)
    print(lgtw_version)
    print()
    ws.send(json.dumps(lgtw_version))

    try:
        lgtw_config = ws.recv()
    except WebSocketConnectionClosedException:
        print("WS connection closed, check if lgtw_euid is a valid value")
        exit()

    print("LoRaWAN GTW config:\n")
    print(lgtw_config)
    print()

    print("Sending packets\n")
    for packet in packets["packets"]:
        print(str(int(datetime.datetime.now().timestamp())) +
              ": " + str(packet))
        ws.send(json.dumps(packet))
        time.sleep(SLEEP_TIME)

    ws.close()


if __name__ == '__main__':
    main()
