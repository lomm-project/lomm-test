# LoMM Test Suite

Allow to test LoMM implementation, using captured datasets to simulate a packet flow from a LoRaWAN Gateway.

## LoMM configuration

Before running the test suite you should configure LoMM with your lGtw information, and you lEndDevs data (if avaliable).

### Add an LNS to the LNS discovery service database

From the `lomm-empower-runtime` directory, use the `add-lns` command from the empower cli:

`./empower-ctl.py --auth <user>:<password> add-lns --lns_euid <LNS euid> --desc <LNS descriptio> --uri <LNS Web Socket connection URI>  --lgtws [<lGtw euid>,..]`

***Example***

`./empower-ctl.py --auth root:root add-lns --lns_euid "0000:0000:0000:0001" --desc "LNS Server" --uri "ws://192.168.28.189:6039/router-"  --lgtws ["0827:ebff:fee7:7681"]`

### Configure lGtw

From the `lomm-empower-runtime` directory, use the `add-lgtw` command from the empower cli:

`./empower-ctl.py --auth <user>:<password> add-lgtw --lgtw_euid <lGtw euid>`

***Example***

`./empower-ctl.py --auth root:root add-lgtw --lgtw_euid "b827:ebff:fee7:7681"`

### Add lEndDevs data

From the `lomm-empower-runtime` directory, use the `add-lenddevs` command from the empower cli:

`./empower-ctl.py --auth <user>:<password> add-lenddevs -j <json file with lEndDevs data>`

***Example***

`./empower-ctl.py --auth root:root add-lenddevs -j ./empower/managers/lommmanager/lnsp/lenddevs.json --auth root:root`

## Web Socket LoMM client Usage

`usage: websocket_test_client.py [-h] [-f FILENAME] [-l LGTW_EUID] [-a LNS_IP] [-p LNS_PORT]`

**optional arguments:**

```
  -h, --help            show this help message and exit
  -f FILENAME, --filename FILENAME
                        Dataset file
                        Default: './dataset/lorawan_capture.json'
  -l LGTW_EUID, --lgtw_euid LGTW_EUID
                        LoRaWAN Gateway euid
                        Default: "b827:ebff:fee7:7681"
  -a LNS_IP, --lns_ip LNS_IP
                        LoRaWAN LNS IP address
                        Default: 127.0.0.1
  -p LNS_PORT, --lns_port LNS_PORT
                        LoRaWAN LNS IP port
                        Default: 6039
```

**Sample output:**

```
Sending version request

{'msgtype': 'version', 'station': 'b827:ebff:fee7:7681', 'firmware': 'v1.0', 'package': 'alpha', 'model': 'Basic Station', 'protocol': '2', 'features': 'xx'}

LoRaWAN GTW config:

{"JoinEui": null, "NetID": null, "nocca": false, "nodwell": false, "nodc": true, "region": "EU863", "hwspec": "sx1301/1", "freq_range": [863000000, 870000000], "DRs": [[12, 125, 0], [11, 125, 0], [10, 125, 0], [9, 125, 0], [8, 125, 0], [7, 125, 0], [6, 250, 0], [0, 0, 0], [-1, 0, 0], [-1, 0, 0], [-1, 0, 0], [-1, 0, 0], [-1, 0, 0], [-1, 0, 0], [-1, 0, 0], [-1, 0, 0]], "sx1301_conf": [{"radio_0": {"enable": true, "freq": 868300000}, "radio_1": {"enable": false, "freq": 0}}], "chan_multiSF_0": {"enable": true, "radio": 0, "if": -200000}, "chan_multiSF_1": {"enable": true, "radio": 0, "if": 0}, "chan_multiSF_2": {"enable": true, "radio": 0, "if": 200000}, "chan_multiSF_3": {"enable": false}, "chan_multiSF_4": {"enable": false}, "chan_multiSF_5": {"enable": false}, "chan_multiSF_6": {"enable": false}, "chan_multiSF_7": {"enable": false}, "chan_FSK": {"enable": false}, "chan_Lora_std": {"enable": false}, "upchannels": [[868100000, 0, 5], [868300000, 0, 5], [868500000, 0, 5], [868850000, 0, 5], [869050000, 0, 5], [869525000, 0, 5]], "bcning": null, "config": {}, "max_eirp": 16.0, "protocol": 1, "regionid": 1002, "msgtype": "router_config", "MuxTime": 1583341897.486047}

Sending packets

1583341897: {'msgtype': 'updf', 'MHdr': 64, 'DevAddr': 3802898, 'FCtrl': 128, 'FCnt': 18183, 'FOpts': '', 'FPort': 1, 'FRMPayload': '7A568C1364C32863B03D2F9B81995225E2DB', 'MIC': 620959691, 'RefTime': 1574323198.329848, 'DR': 5, 'Freq': 868100000, 'upinfo': {'rctx': 0, 'xtime': 2533274825217323, 'gpstime': 0, 'rssi': -57, 'snr': 8.5, 'rxtime': 1574323198.33001}}
1583341899: {'msgtype': 'updf', 'MHdr': 64, 'DevAddr': 637605455, 'FCtrl': 128, 'FCnt': 6, 'FOpts': '', 'FPort': 1, 'FRMPayload': '6E2A3175C8487D97BED2B2', 'MIC': 1779714674, 'RefTime': 1574323350.791826, 'DR': 5, 'Freq': 868500000, 'upinfo': {'rctx': 0, 'xtime': 37999121859851107, 'gpstime': 0, 'rssi': -30, 'snr': 9, 'rxtime': 1574323350.791928}}
```

## Datasets

Packets captured in json format can be found in the `./dataset` directory.
